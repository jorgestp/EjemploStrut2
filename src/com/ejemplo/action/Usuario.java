package com.ejemplo.action;

import com.opensymphony.xwork2.ActionSupport;

public class Usuario extends ActionSupport {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userlogin;
	private String userPass;
	private String userNombre;
	
	public String execute() {
		String ir = ERROR;
		
		if(getUserlogin().equals("jorge") && getUserPass().equals("123")) {
			
			userNombre = "JORGE VILLALBA";
			ir =SUCCESS;
			return ir;
		}
		return ir;
		
	}
	
	
	public String getUserlogin() {
		return userlogin;
	}
	public void setUserlogin(String userlogin) {
		this.userlogin = userlogin;
	}
	public String getUserPass() {
		return userPass;
	}
	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}
	public String getUserNombre() {
		return userNombre;
	}
	public void setUserNombre(String userNombre) {
		this.userNombre = userNombre;
	}

}
